const coordinates = [];

const lineWidth = 2;
const lineColor = 'red';
const magnetRange = 5;

Number.prototype.between = function (min, max) {
    return this > min && this < max;
};

function createCanvas() {
    const image = document.getElementById("image1");
    image.parentNode.innerHTML =
        '<canvas id="canvas1" onclick="draw(event, this)"></canvas>';
    const canvas = document.getElementById("canvas1");
    canvas.style = 'cursor:crosshair';
    canvas.width = image.width;
    canvas.height = image.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(image, 0,0);
}

function draw(event, surface) {
    const canvas = document.getElementById("canvas1");
    const ctx = canvas.getContext('2d');
    ctx.strokeStyle = lineColor;
    ctx.fillStyle = lineColor;
    ctx.lineWidth = lineWidth;
    const posX = event.offsetX ? (event.offsetX) : event.pageX - surface.offsetLeft;
    const posY = event.offsetY ? (event.offsetY) : event.pageY - surface.offsetTop;
    coordinates.push({"x": posX, "y": posY});
    const LastItem =  coordinates[coordinates.length - 1];
    ctx.beginPath();
    ctx.moveTo(coordinates[0].x, coordinates[0].y);
    ctx.fillRect(coordinates[0].x - magnetRange, coordinates[0].y - magnetRange, magnetRange*2, magnetRange*2);
    const checkX = posX.between(coordinates[0].x - magnetRange, coordinates[0].x + magnetRange);
    const checkY = posY.between(coordinates[0].y - magnetRange, coordinates[0].y + magnetRange);
    for (var i = 1; i < coordinates.length; i++) {
        if (i === coordinates.length - 1 && checkX && checkY) {
            LastItem.x = coordinates[0].x;
            LastItem.y = coordinates[0].y;
            ctx.closePath();
        } else {
            ctx.lineTo(coordinates[i].x, coordinates[i].y);
            ctx.fillRect(coordinates[i].x - magnetRange, coordinates[i].y - magnetRange, magnetRange * 2, magnetRange * 2);
        }
    }
    ctx.stroke();
    console.log(coordinates)
}